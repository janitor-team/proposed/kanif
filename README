kanif - a TakTuk wrapper for cluster management

DESCRIPTION:

       kanif is a tool for cluster management and administration. It
       combines main features of well known cluster management tools
       such as c3, pdsh and dsh and mimics their syntax. For the effective
       cluster management it relies on TakTuk, a tool for large scale remote
       execution deployment.

       For simple parallel tasks that have to be executed on regular machines
       such as clusters, TakTuk syntax is too complicated.  The goal of
       kanif is to provide an easier and familiar syntax to cluster
       administrators while still taking advantage of TakTuk characteristics
       and features (adaptivity, scalability, portability, autopropagation and
       informations redirection).

       To work, kanif needs to find the "taktuk" command (version 3.2.3
       and above) in the user path. The other requirements are the same as
       TakTuk: it requires, on all the nodes of the cluster, a working Perl
       interpreter (version 5.8 and above) and a command to log without pass-
       word (such as "ssh" with proper rsa keys installed).

       kanif provides three simple commands for clusters administration
       and management:

       kash: runs the same command on multiple nodes
       kaput: broadcasts the copy of files or directories to several nodes
       kaget: gathers several remote files or directories

       kanif combines the advantages of several cluster management
       tools.  Its main features can be summarized as follows:

       * C3-style configuration file for static clusters setups
       * pdsh-like options such as nodes ranges and timeouts
       * dshbak-like gathering, sorting and merging of output

       kanif also reports a quick summary of failures: connections and
       commands execution.

AUTHORS:

The author of kanif and current maintainer of the package is
Guillaume Huard. Acknowledgements to Lucas Nussbaum for the idea of the name
"kanif".

COPYRIGHT:

kanif is provided under the terms of the GNU General Public License
version 2 or later.
